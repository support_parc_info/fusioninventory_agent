# Installation du micro K8S

<br>
<br>

## Créer la connexion vers OpenStack

Il est nécessaire de paramétrer le serveur **Ansible** pour ce connecter vers **OpenStack**.
Pour ce faire un faut installer un fichier de paramétrage *.config/openstack/clouds.yml*

```code
clouds:
  mt-a.bernard:
    auth:
      auth_url: https://iaas.monaco-telecom.mc:13000
      username: mt-a.bernard.ansible
      password: xxxxxxxx
      project_name: mt-a.bernard
      domain_id: 860492b51bdb4ecd85a1f8745d9a80f4
    region_name: "regionOne"
    interface: "public"
    identity_api_version: 3
```

<br>
<br>


## Renseigner les variables d'installation du cluster

Avant de démarrer la création des instances renseigner les variables dans le fichier *kubernetes/inventory/group_vars/all.yml*
Les variables propres à chaques roles ce trouvent dans **vars** du role en question.


```code

## Variables pour la création des instances OpenStack

# OpenStack Projet
projet_cloud: mt-a.bernard

# Image OpenStack (OS)
# nom_image: CentOS-Stream-8
nom_image: CentOS-8

# Zone de disponibilité des instances
zone_disponibilite: fontvieille-dc3

# Gabarits des instances OpenStack
gabarit_master: s-4880-month
gabarit_node: s-4880-month 

# Reseaux associé au routeur OpenStack pour mapper le port 
public_network: Internet

# Nom des instances et adresses privées
nom_instance_master: k8smaster
private_ip_master: 192.168.1.50

nom_instance_node: k8sworker
private_ip_node: 192.168.1.5

nom_instance_rancher: rancher
private_ip_rancher: 192.168.1.6


# Nombre d'instances worker
nombre_de_workers: 
- 1
- 2
- 3

# Nombre d'instance rancher
nombre_de_ranchers: 
- 1


# Type d'instance deployé 'master' ou 'node' à changer avant d'exécuter les playbooks 'setup_master' ou 'setup_worker'
type: node

# Prefix utilisé pour nommer les objects OpenStack. Exemple, "{{ application }}_subnet" ou "{{ application }}_router"
application: kube
```

<br>
<br>

## Création du node **master** dans OpenStack

<br>

### Prérequis hardware, pour la création du **master** K8S   

<br>

A minimum Kubernetes master node configuration is:

    4 CPU cores (Intel VT-capable CPU)
    16GB RAM.
    1GB Ethernet NIC.
    40GB hard disk space in the /var directory.

Le gabarit disponinble pour le master et les nodes est le `s-416160-month` soit *4VCPUs*,*16GO* de mémoire,*160GO* de disque.

<br>

Renseigner le type d'installation dans le fichier *kubernetes/inventory/group_vars/all.yml*
Les types sont **master** ou **node**

    # Type d'instance deployé 'master' ou 'node' à changer avant d'exécuter les playbooks 'setup_master' ou 'setup_worker'
    type: master



S'il sagit d'une premiére exécution des rôles utiliser le tag *create_floating_ip* s'il s'agit d'une ré exécution des rôles de création des instances dans **OpenStack** utiliser le tag  *assign_floating_ip*


Puis récupérer les adresses une fois le rôle de création de l'instance éxécuté afin que celles ci soient joignables pour les rôles de configurations. Moi je les ajoutent dans mon fichier *hosts* pendant la phase de création.


<br>
<br>

### Instanciation OpensStack, pour la création du **master** K8S   

<br>

`PENSER A POSITIONER LA VALEUR master DANS LE FICHIER ALL`


`NOTE` Sinon utiliser la précédence des variables avec le paramétre --extra-vars dans la ligne de commande

    ansible-playbook -i inventory/hosts setup_openstack.yml --extra-vars "type=master" --tags network,subnet,router,port,create_floating_ip,instance,create_floating_ip

<br>
<br>

Lors de la premiére excécution du playbook, il faut crééer les régles d'accés dans ce cas ajouter le tag 'security_group'

    ansible-playbook -i inventory/hosts setup_openstack.yml --extra-vars "type=master" --tags network,subnet,router,port,create_floating_ip,instance,assign_floating_ip,security_group

<br>


Sans le tag 'security_group'

<br>


```code  
ansible-playbook -i inventory/hosts setup_openstack.yml --extra-vars "type=master" --tags network,subnet,router,port,create_floating_ip,instance,assign_floating_ip

PLAY [localhost] ***********************************************************************************************************************************************
TASK [Gathering Facts] *****************************************************************************************************************************************
ok: [localhost]

TASK [Création du réseaux] *************************************************************************************************************************************
changed: [localhost]

TASK [Création du sous réseaux] ********************************************************************************************************************************
changed: [localhost]

TASK [Create router] *******************************************************************************************************************************************
changed: [localhost]

TASK [Create admin Network Port for K8s master] ****************************************************************************************************************
changed: [localhost]

TASK [Create admin Network Port for K8s worker] ****************************************************************************************************************
skipping: [localhost]

TASK [Create floating ip] **************************************************************************************************************************************
changed: [localhost]

TASK [Create k8smaster instances] ******************************************************************************************************************************
changed: [localhost]

TASK [Create Node Kubernetes instances] ************************************************************************************************************************
skipping: [localhost]

TASK [Assign floating ip pool to master] ***********************************************************************************************************************
changed: [localhost]

TASK [Assign floating ip pool to worker] ***********************************************************************************************************************
skipping: [localhost]

PLAY RECAP *****************************************************************************************************************************************************
localhost                  : ok=8    changed=7    unreachable=0    failed=0    skipped=3    rescued=0    ignored=0
```

<br>
<br>

### Création du node **node** dans OpenStack

<br>

`PENSER A POSITIONER LA VALEUR node DANS LE FICHIER ALL`

<br>


Renseigner le type d'installation dans le fichier *kubernetes/inventory/group_vars/all.yml*
Les types sont **master** ou **node**

    # Type d'instance deployé 'master' ou 'node' à changer avant d'exécuter les playbooks 'setup_master' ou 'setup_worker'
    type: node

<br>


Instanciation OpensStack, pour la création du **node** K8S.
Si on souhaitent ré assigner des adresses IP déja présentent dans les **foatings_ip** choisir l'option *create_floating_ip* plutot que *assign_floating_ip*  

```code  
ansible-playbook -i inventory/hosts setup_openstack.yml  --extra-vars "type=node" --tags network,subnet,router,port,create_floating_ip,instance,create_floating_ip

PLAY [localhost] **********************************************************************************************************************************************************************************

TASK [Gathering Facts] ****************************************************************************************************************************************************************************
ok: [localhost]

TASK [Création du réseaux] ************************************************************************************************************************************************************************
ok: [localhost]

TASK [Création du sous réseaux] *******************************************************************************************************************************************************************
ok: [localhost]

TASK [Create router] ******************************************************************************************************************************************************************************
ok: [localhost]

TASK [Create admin Network Port for K8s master] ***************************************************************************************************************************************************
skipping: [localhost]

TASK [Create admin Network Port for K8s worker] ***************************************************************************************************************************************************
changed: [localhost]

TASK [Create floating ip] *************************************************************************************************************************************************************************
changed: [localhost]

TASK [Create k8smaster instances] *****************************************************************************************************************************************************************
skipping: [localhost]

TASK [Create Node Kubernetes instances] ***********************************************************************************************************************************************************
changed: [localhost]

TASK [Assign floating ip pool to master] **********************************************************************************************************************************************************
skipping: [localhost]

TASK [Assign floating ip pool to worker] **********************************************************************************************************************************************************
changed: [localhost]

PLAY RECAP ****************************************************************************************************************************************************************************************
localhost                  : ok=8    changed=4    unreachable=0    failed=0    skipped=3    rescued=0    ignored=0```
```


<br>
<br>

### Installation de Docker

<br>

Afin que le playbook soit plus modulaire le rôle Docker est indépendant du reste de l'installation. 

<br>

Exécution du rôle **docker**


```code
$ ansible-playbook -i inventory/hosts.yml setup_docker.yml                                   

PLAY [all] **********************************************************************************************************************************************************************************************************************************************************

TASK [Gathering Facts] **********************************************************************************************************************************************************************************************************************************************
ok: [k8smaster]
ok: [k8sworker-3]
ok: [k8sworker-2]
ok: [rancher-1]
ok: [k8sworker-1]

TASK [commun : Désactivation de la swap] ****************************************************************************************************************************************************************************************************************************
changed: [k8sworker-1]
changed: [k8sworker-3]
changed: [k8sworker-2]
changed: [rancher-1]
changed: [k8smaster]

TASK [commun : Commentaire dans '/etc/fstab' de la swap] ************************************************************************************************************************************************************************************************************
ok: [k8smaster]
ok: [k8sworker-1]
ok: [k8sworker-3]
ok: [rancher-1]
ok: [k8sworker-2]

TASK [commun : Add IPs dans /etc/hosts sur le master et les workers] ************************************************************************************************************************************************************************************************
changed: [k8sworker-2] => (item=k8smaster)
changed: [rancher-1] => (item=k8smaster)
changed: [k8sworker-1] => (item=k8smaster)
changed: [k8sworker-3] => (item=k8smaster)
changed: [k8smaster] => (item=k8smaster)
changed: [k8sworker-1] => (item=k8sworker-1)
changed: [k8sworker-2] => (item=k8sworker-1)
changed: [rancher-1] => (item=k8sworker-1)
changed: [k8smaster] => (item=k8sworker-1)
changed: [k8sworker-3] => (item=k8sworker-1)
changed: [k8sworker-1] => (item=k8sworker-2)
changed: [rancher-1] => (item=k8sworker-2)
changed: [k8smaster] => (item=k8sworker-2)
changed: [k8sworker-2] => (item=k8sworker-2)
changed: [k8sworker-3] => (item=k8sworker-2)
changed: [k8sworker-1] => (item=k8sworker-3)
changed: [rancher-1] => (item=k8sworker-3)
changed: [k8smaster] => (item=k8sworker-3)
changed: [k8sworker-2] => (item=k8sworker-3)
changed: [k8sworker-3] => (item=k8sworker-3)
changed: [k8sworker-1] => (item=rancher-1)
changed: [rancher-1] => (item=rancher-1)
changed: [k8sworker-3] => (item=rancher-1)
changed: [k8sworker-2] => (item=rancher-1)
changed: [k8smaster] => (item=rancher-1)

TASK [docker : Ajout dépôt Docker] **********************************************************************************************************************************************************************************************************************************
changed: [k8sworker-1]
changed: [rancher-1]
changed: [k8sworker-2]
changed: [k8sworker-3]
changed: [k8smaster]

TASK [docker : Installation des paquets] ****************************************************************************************************************************************************************************************************************************
changed: [k8sworker-2]
changed: [k8sworker-1]
changed: [k8sworker-3]
changed: [k8smaster]
changed: [rancher-1]

TASK [docker : Activation des services 'Docker'] ********************************************************************************************************************************************************************************************************************
changed: [k8sworker-1] => (item=docker)
changed: [k8smaster] => (item=docker)
changed: [rancher-1] => (item=docker)
changed: [k8sworker-2] => (item=docker)
changed: [k8sworker-3] => (item=docker)
changed: [rancher-1] => (item=iscsid)
changed: [k8sworker-3] => (item=iscsid)
changed: [k8smaster] => (item=iscsid)
changed: [k8sworker-1] => (item=iscsid)
changed: [k8sworker-2] => (item=iscsid)

TASK [docker : Création du fichier 'daemon.json' paramétrage de 'systemd'] ******************************************************************************************************************************************************************************************
changed: [rancher-1]
changed: [k8smaster]
changed: [k8sworker-1]
changed: [k8sworker-3]
changed: [k8sworker-2]

PLAY RECAP **********************************************************************************************************************************************************************************************************************************************************
k8smaster                  : ok=8    changed=6    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0   
k8sworker-1                : ok=8    changed=6    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0   
k8sworker-2                : ok=8    changed=6    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0   
k8sworker-3                : ok=8    changed=6    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0   
rancher-1                  : ok=8    changed=6    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0   

```

<br>
<br>


## Création du master Kubernetes

<br>

Configuration du master Kubernetes.

Si besoin exécuter cette commmande pour nettoyer le fichier *know_hosts*

```code
ssh-keygen -f "/home/ange/.ssh/known_hosts" -R "k8sworker-1" ; ssh-keygen -f "/home/ange/.ssh/known_hosts" -R "k8smaster" ; ssh-keygen -f "/home/ange/.ssh/known_hosts" -R "195.78.23.57" ; ssh-keygen -f "/home/ange/.ssh/known_hosts" -R "195.78.22.47" ; ssh k8smaster df ; ssh k8sworker-1 df ; ssh-keygen -f "/home/ange/.ssh/known_hosts" -R "rancher" ; ssh-keygen -f "/home/ange/.ssh/known_hosts" -R "195.78.23.171" ; ssh-keygen -f "/home/ange/.ssh/known_hosts" -R "rancher" ; ssh 195.78.23.171 "df"
```

<br>

Puis lancer un **ping** Ansible avant de d'éxécuter la configuration du cluster

```code
ansible -i inventory/hosts master,worker -m ping
k8sworker-1 | SUCCESS => {
    "ansible_facts": {
        "discovered_interpreter_python": "/usr/libexec/platform-python"
    },
    "changed": false,
    "ping": "pong"
}
k8smaster | SUCCESS => {
    "ansible_facts": {
        "discovered_interpreter_python": "/usr/libexec/platform-python"
    },
    "changed": false,
    "ping": "pong"
}
```

<br>

Voilà le playboook de configuration peu être éxécuté.

```code
ansible-playbook -i inventory/hosts master setup_master.yml

PLAY [all] ***********************************************************************************************************************************************************************************************
TASK [Gathering Facts] ***********************************************************************************************************************************************************************************ok: [k8sworker-1]
ok: [k8smaster]

TASK [Désactivation de la swap] **************************************************************************************************************************************************************************
changed: [k8smaster]
changed: [k8sworker-1]

TASK [Commentaire dans '/etc/fstab' de la swap] **********************************************************************************************************************************************************
ok: [k8sworker-1]
ok: [k8smaster]

TASK [Add IPs dans /etc/hosts sur le master et les workers] **********************************************************************************************************************************************
changed: [k8sworker-1] => (item=k8smaster)
changed: [k8smaster] => (item=k8smaster)
changed: [k8sworker-1] => (item=k8sworker-1)
changed: [k8smaster] => (item=k8sworker-1)

PLAY [all] ***********************************************************************************************************************************************************************************************
TASK [Gathering Facts] ***********************************************************************************************************************************************************************************
ok: [k8smaster]
ok: [k8sworker-1]

TASK [Création du fichier Yum pour l'installation de 'Kubernetes'] ***************************************************************************************************************************************
changed: [k8smaster]
changed: [k8sworker-1]

TASK [Configuration de la définition Yum du dépôt 'kubernetes'] ******************************************************************************************************************************************
changed: [k8smaster]
changed: [k8sworker-1]

TASK [Ajout dépôt Docker] ********************************************************************************************************************************************************************************
changed: [k8smaster]
changed: [k8sworker-1]

TASK [Installation des paquets] **************************************************************************************************************************************************************************
changed: [k8smaster]
changed: [k8sworker-1]

TASK [Activation des services 'Docker'] ******************************************************************************************************************************************************************
changed: [k8smaster] => (item=docker)
changed: [k8sworker-1] => (item=docker)
changed: [k8smaster] => (item=kubelet)
changed: [k8sworker-1] => (item=kubelet)

TASK [Création du fichier 'daemon.json' paramétrage de 'systemd'] ****************************************************************************************************************************************
changed: [k8smaster]
changed: [k8sworker-1]

PLAY [master] ********************************************************************************************************************************************************************************************

TASK [Gathering Facts] ***********************************************************************************************************************************************************************************
ok: [k8smaster]

TASK [Pulling images cluster Kubernetes] *****************************************************************************************************************************************************************
changed: [k8smaster]

TASK [Activation des services 'Docker'] ******************************************************************************************************************************************************************
changed: [k8smaster]

TASK [Initialisation du cluster Kubernetes cluster] ************************************************************************************************************************************************************
changed: [k8smaster]

TASK [Stockage du token dans un fichier locale 'join_token'] ***************************************************************************************************************************************************
changed: [k8smaster]

TASK [Création du répertoire '.kube'] **************************************************************************************************************************************************************************
changed: [k8smaster]

TASK [Copie du fichier 'config' de kube dans le home dir] ******************************************************************************************************************************************************
changed: [k8smaster]

TASK [Installation de l'addon réseaux] *************************************************************************************************************************************************************************
changed: [k8smaster]

TASK [Installation de la complétion 'kubernetes'] **************************************************************************************************************************************************************
changed: [k8smaster]

TASK [Add '/usr/share/bash-completion/bash_completion' dans '.bashrc'] *****************************************************************************************************************************************
changed: [k8smaster]

TASK [Ajout des droits en excecution sur le script 'bash_completion'] ******************************************************************************************************************************************
changed: [k8smaster]

TASK [Configuration de la complétion Kubernetes] ***************************************************************************************************************************************************************
changed: [k8smaster]

PLAY RECAP *****************************************************************************************************************************************************************************************************
k8smaster                  : ok=23   changed=16   unreachable=0    failed=0    skipped=0    rescued=0    ignored=0
k8sworker-1                : ok=11   changed=4    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0

```
<br>
<br>

### Création du node Kubernetes

<br>

```code
ansible-playbook -i inventory/hosts worker setup_worker.yml

PLAY [all] *****************************************************************************************************************************************************************************************************

TASK [Gathering Facts] *****************************************************************************************************************************************************************************************
ok: [k8sworker-1]
ok: [k8smaster]

TASK [Désactivation de la swap] ********************************************************************************************************************************************************************************
changed: [k8sworker-1]
changed: [k8smaster]

TASK [Commentaire dans '/etc/fstab' de la swap] ****************************************************************************************************************************************************************
ok: [k8sworker-1]
ok: [k8smaster]

TASK [Add IPs dans /etc/hosts sur le master et les workers] ****************************************************************************************************************************************************
ok: [k8sworker-1] => (item=k8smaster)
ok: [k8smaster] => (item=k8smaster)
ok: [k8sworker-1] => (item=k8sworker-1)
ok: [k8smaster] => (item=k8sworker-1)

PLAY [all] *****************************************************************************************************************************************************************************************************

TASK [Gathering Facts] *****************************************************************************************************************************************************************************************
ok: [k8sworker-1]
ok: [k8smaster]

TASK [Création du fichier Yum pour l'installation de 'Kubernetes'] *********************************************************************************************************************************************
changed: [k8sworker-1]
changed: [k8smaster]

TASK [Configuration de la définition Yum du dépôt 'kubernetes'] ************************************************************************************************************************************************
ok: [k8sworker-1]
ok: [k8smaster]

TASK [Ajout dépôt Docker] **************************************************************************************************************************************************************************************
changed: [k8sworker-1]
changed: [k8smaster]

TASK [Installation des paquets] ********************************************************************************************************************************************************************************
ok: [k8smaster]
ok: [k8sworker-1]

TASK [Activation des services 'Docker'] ************************************************************************************************************************************************************************
changed: [k8sworker-1] => (item=docker)
changed: [k8sworker-1] => (item=kubelet)
changed: [k8smaster] => (item=docker)
changed: [k8smaster] => (item=kubelet)

TASK [Création du fichier 'daemon.json' paramétrage de 'systemd'] **********************************************************************************************************************************************
ok: [k8sworker-1]
ok: [k8smaster]

PLAY [worker] **************************************************************************************************************************************************************************************************

TASK [Gathering Facts] *****************************************************************************************************************************************************************************************
ok: [k8sworker-1]

TASK [Copying token to worker nodes] ***************************************************************************************************************************************************************************
changed: [k8sworker-1]

TASK [Activation des services 'Docker'] ************************************************************************************************************************************************************************
changed: [k8sworker-1]

TASK [Joining worker nodes with kubernetes master] *************************************************************************************************************************************************************
changed: [k8sworker-1]

PLAY [master] **************************************************************************************************************************************************************************************************

TASK [Gathering Facts] *****************************************************************************************************************************************************************************************
ok: [k8smaster]

TASK [Label le worker 'worker-x'  "kubectl label nodes worker-1 kubernetes.io/role=worker"] ********************************************************************************************************************
changed: [k8smaster]

TASK [Téléchargement de 'l'addon réseaux Calico'] **************************************************************************************************************************************************************
changed: [k8smaster]

TASK [Installation de l'addon réseaux  'Calico'] ***************************************************************************************************************************************************************
changed: [k8smaster]

TASK [Installation de wget] ************************************************************************************************************************************************************************************
changed: [k8smaster]

TASK [Téléchargement de la commande 'kubeseal'] ****************************************************************************************************************************************************************
changed: [k8smaster]

TASK [Paramétrage des permissions de 'kubeseal'] ***************************************************************************************************************************************************************
changed: [k8smaster]

TASK [Download le sealed controler] ****************************************************************************************************************************************************************************
changed: [k8smaster]

TASK [Création du 'clusterrolebinding' de 'sealedsecret controler'] ********************************************************************************************************************************************
changed: [k8smaster]

TASK [Create a Deployment 'sealed controler'] ******************************************************************************************************************************************************************
changed: [k8smaster]

PLAY [master] **************************************************************************************************************************************************************************************************

TASK [Gathering Facts] *****************************************************************************************************************************************************************************************
ok: [k8smaster]

TASK [Copy des rbac 'Traefik'] *********************************************************************************************************************************************************************************
changed: [k8smaster]

TASK [Configuration des rbac 'Traefik'] ************************************************************************************************************************************************************************
changed: [k8smaster]

TASK [Copy des services 'Traefik'] *****************************************************************************************************************************************************************************
changed: [k8smaster]

TASK [Configuration des services 'Traefik'] ********************************************************************************************************************************************************************
changed: [k8smaster]

TASK [Copy du deployment 'Traefik'] ****************************************************************************************************************************************************************************
changed: [k8smaster]

TASK [Configuration du deployment 'Traefik'] *******************************************************************************************************************************************************************
changed: [k8smaster]

PLAY RECAP *****************************************************************************************************************************************************************************************************
k8smaster                  : ok=28   changed=19   unreachable=0    failed=0    skipped=0    rescued=0    ignored=0
k8sworker-1                : ok=15   changed=7    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0

```

<br>

Voilà le cluster **Kubernetes** est installé et prét à orquestrer les applications déployées

<br>

```code
k get nodes
NAME          STATUS   ROLES    AGE     VERSION
k8smaster     Ready    master   9m48s   v1.19.15
k8sworker-1   Ready    worker   6m15s   v1.19.15
```

<br>
<br>


## Déploiement de Rancher

<br>

- **Rancher** est une application qui permet via une *weui* de déployer des infrastructures **Kubernetes** et des applications au sein des cluster **K8**

<br>

### Déploiement de l'instance dans **OpenStack**

<br>

```code
ansible-playbook -i inventory/hosts setup_openstack.yml --tags port_rancher,rancher,assign_floating_ip_rancher

PLAY [localhost] ************************************************************************************************************************************************************************************************************************************************

TASK [Gathering Facts] ******************************************************************************************************************************************************************************************************************************************
ok: [localhost]

TASK [Create admin Network Port for Rancher] ********************************************************************************************************************************************************************************************************************
changed: [localhost]

TASK [Create Rancher instances] *********************************************************************************************************************************************************************************************************************************
changed: [localhost]

TASK [Assign floating ip pool to rancher] ***********************************************************************************************************************************************************************************************************************
changed: [localhost]

PLAY RECAP ******************************************************************************************************************************************************************************************************************************************************
localhost                  : ok=4    changed=3    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0
```

<br>
<br>

### Configuration de Rancher

<br>

L'installation de **Rancher** est basé sur un container **docker**

<br>

```code
ansible-playbook -i inventory/hosts setup_rancher.yml

PLAY [rancher] **************************************************************************************************************************************************************************************************************************************************

TASK [Gathering Facts] ******************************************************************************************************************************************************************************************************************************************
ok: [rancher]

TASK [Install packages 'yum-utils' 'net-tools' 'vim'] ***********************************************************************************************************************************************************************************************************
changed: [rancher]

TASK [Add repository 'Docker'] **********************************************************************************************************************************************************************************************************************************
changed: [rancher]

TASK [Install 'Docker' packages] ********************************************************************************************************************************************************************************************************************************
changed: [rancher]

TASK [Démarrage du service 'Docker'] ****************************************************************************************************************************************************************************************************************************
changed: [rancher]

TASK [Activation du service 'Docker'] ***************************************************************************************************************************************************************************************************************************
changed: [rancher]

TASK [Démarrage du container "Rancher"] *************************************************************************************************************************************************************************************************************************
changed: [rancher]

PLAY RECAP ******************************************************************************************************************************************************************************************************************************************************
rancher                    : ok=7    changed=6    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0
```

<br>

Une fois le container déployé il faut récupérer le mot de passe de l'utilisateur *admin* dans le fichier *rancher_pass*

<br>

`NOTE`

Ce connecter sur la page d'acceuil de `rancher`,  puis récupérer le mot de passe initial en lancant la commande suivante:

```code
ansible-playbook -i inventory/hosts setup_rancher.yml --tags pass

PLAY [rancher] **************************************************************************************************************************************************************************************************************************************************

TASK [Gathering Facts] ******************************************************************************************************************************************************************************************************************************************
ok: [rancher]

TASK [get container ID "Rancher"] *******************************************************************************************************************************************************************************************************************************
changed: [rancher]

TASK [Récupération du mot de passe initial de "Rancher"] ********************************************************************************************************************************************************************************************************
changed: [rancher]

TASK [Stockage du mot de passe de "Rancher" dans un fichier locale 'rancher_pass'] ******************************************************************************************************************************************************************************
changed: [rancher]

PLAY RECAP ******************************************************************************************************************************************************************************************************************************************************
rancher                    : ok=4    changed=3    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0
```

<br>

Récupération de mot de passe  dans le fichier locale *rancher_pass* 

```code
cat rancher_pass
2021/11/11 08:21:06 [INFO] Bootstrap Password: pv7bhl9v6tgdrnfp9wjwrgltlwxzcghmgjp8f9cjphpn7kk9lwhw7f
```

<br>
<br>

## Déploiement d'une application de test

<br>

```code
ansible-playbook -i inventory/hosts setup_wordpress.yml

PLAY [master] **************************************************************************************************************************************************************************************************

TASK [Gathering Facts] *****************************************************************************************************************************************************************************************
ok: [k8smaster]

TASK [Copy du manifest de création des volumes 'Mysql'] ********************************************************************************************************************************************************
changed: [k8smaster]

TASK [Configuration des volumes 'Mysql'] ***********************************************************************************************************************************************************************
changed: [k8smaster]

TASK [Copy du manifest des secrets 'Mysql'] ********************************************************************************************************************************************************************
changed: [k8smaster]

TASK [Configuration des secrets 'Mysql'] ***********************************************************************************************************************************************************************
changed: [k8smaster]

TASK [Copy du manifest du deploiement 'Mysql'] *****************************************************************************************************************************************************************
changed: [k8smaster]

TASK [Configuration du déploiement 'Mysql'] ********************************************************************************************************************************************************************
changed: [k8smaster]

TASK [Copy du manifest de création des volumes de 'WordPress'] *************************************************************************************************************************************************
changed: [k8smaster]

TASK [Configuration des volumes de 'WordPress'] ****************************************************************************************************************************************************************
changed: [k8smaster]

TASK [Copy du manifest de déploiement de 'WordPress'] **********************************************************************************************************************************************************
changed: [k8smaster]

TASK [Configuration du déploiement 'WordPress'] ****************************************************************************************************************************************************************
changed: [k8smaster]

PLAY RECAP *****************************************************************************************************************************************************************************************************
k8smaster                  : ok=11   changed=10   unreachable=0    failed=0    skipped=0    rescued=0    ignored=0
```

<br>

L'application **Wordpress** est déployée et joignable


```code 
k get all
NAME                                    READY   STATUS    RESTARTS   AGE
pod/partenaire-mysql-76bc458947-rxqqs   1/1     Running   0          2m11s
pod/traefik-5db84589db-nt5fl            1/1     Running   0          7m20s
pod/wordpress-5cbd6df58d-fxz98          1/1     Running   0          2m9s

NAME                      TYPE        CLUSTER-IP     EXTERNAL-IP   PORT(S)                       AGE
service/kubernetes        ClusterIP   10.96.0.1      <none>        443/TCP                       31m
service/service-mysql     ClusterIP   None           <none>        3306/TCP                      2m11s
service/service-traefik   NodePort    10.98.16.31    <none>        80:30021/TCP,8080:30042/TCP   27m
service/wordpress         ClusterIP   10.103.241.6   <none>        80/TCP                        2m9s

NAME                               READY   UP-TO-DATE   AVAILABLE   AGE
deployment.apps/partenaire-mysql   1/1     1            1           2m11s
deployment.apps/traefik            1/1     1            1           7m20s
deployment.apps/wordpress          1/1     1            1           2m9s

NAME                                          DESIRED   CURRENT   READY   AGE
replicaset.apps/partenaire-mysql-76bc458947   1         1         1       2m11s
replicaset.apps/traefik-5db84589db            1         1         1       7m20s
replicaset.apps/wordpress-5cbd6df58d          1         1         1       2m9s
```

<br>

L'Ingress controler écoute sur le port `30021`.

```code
lynx wordpress.monaco-telecom.biz:30021
```

```code                                                                                                                                                                                                                                        WordPress › Installation   WordPress
   Select a default language [English (United States)________]

   Continue
```

<br>
<br>

## Purge de l'infrastructure 

<br>

Suppression des instances **OpenStack**  
 - Purge des instances **OpenStack**.
 - Purge des ports des instances qui sont attachées au routeur.
 - Purge du router.
 - Purge du réseaux.

<br>
<br>



### Suppression de l'instances OpenStack **Rancher**

<br>


- Suppression de l'instance **OpenStack** **Rancher**
- Suppression du port de l'instance **Rancher**

Si besoin de tous ré installer commencer par supprimer l'instance **rancher**

```code
ansible-playbook -i inventory/hosts setup_openstack.yml --tags purge_rancher,purge_port_rancher
[WARNING]: Found both group and host with same name: rancher

PLAY [localhost] ************************************************************************************************************************************************************************************************************************************************

TASK [Gathering Facts] ******************************************************************************************************************************************************************************************************************************************
ok: [localhost]

TASK [Purge Rancher instances] **********************************************************************************************************************************************************************************************************************************
changed: [localhost]

TASK [Purge admin Network Port for "Rancher"] *******************************************************************************************************************************************************************************************************************
changed: [localhost]

PLAY RECAP ******************************************************************************************************************************************************************************************************************************************************
localhost                  : ok=3    changed=2    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0
```

<br>
<br>

### Suppression des instances OpenStack **master** et **node**

<br>

Purge de tout les elements y compris les security group:

    ansible-playbook -i inventory/hosts setup_openstack.yml --tags purge_master,purge_node,purge_port_master,purge_port_node,purge_router,purge_network,delete_security_group

<br>

Purge de tout les éléments mais conservation des security_group:

```code
ansible-playbook -i inventory/hosts setup_openstack.yml --tags purge_master,purge_node,purge_port_master,purge_port_node,purge_router,purge_network

PLAY [localhost] **************************************************************************************************************************************************************************************************************************************************

TASK [Gathering Facts] ********************************************************************************************************************************************************************************************************************************************
ok: [localhost]

TASK [Purge Instances Master] *************************************************************************************************************************************************************************************************************************************
changed: [localhost]

TASK [Purge Instances Node] ***************************************************************************************************************************************************************************************************************************************
changed: [localhost]

TASK [Purge admin Network Port for K8s master] ********************************************************************************************************************************************************************************************************************
changed: [localhost]

TASK [Purge admin Network Port for K8s worker] ********************************************************************************************************************************************************************************************************************
changed: [localhost]

TASK [Purge router] ***********************************************************************************************************************************************************************************************************************************************
changed: [localhost]

TASK [Purge Network] **********************************************************************************************************************************************************************************************************************************************
changed: [localhost]

PLAY RECAP ********************************************************************************************************************************************************************************************************************************************************
localhost                  : ok=7    changed=6    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0

```

<br>
<br>

### Update du cluster Kubernetes **master** et **node**

<br>

   Afficher les versions de **kubernetes** disponibles, il est conseilé de monter les versions de une majeur à la fois.
   Commencer par le master puis conséqutivement sur les **workers**. Une fois la version montée recommencer jusqu'à à arriver à la version souhaitée.

   Utiliser le tag **display_versions** pour afficher les versions.



```code
ange@ubuntu:~/ansible/playbooks/microk8s$ ansible-playbook -i inventory/hosts playbooks/update_master.yml --tags display_versions                                                                                                                                             
                                                                                                                                                                                                                                                                              
PLAY [master] ****************************************************************************************************************************************************************************************************************************************************************
                                                                                                                                                                                                                                                                              
TASK [Gathering Facts] *******************************************************************************************************************************************************************************************************************************************************
ok: [k8smaster]                                                                                                                                                                                                                                                               
                                                                                                                                                                                                                                                                              
TASK [Get kubernetes versions] ***********************************************************************************************************************************************************************************************************************************************
changed: [k8smaster]                                                                                                                                                                                                                                                          
                                                                                                                                                                                                                                                                              
TASK [Print kubernetes versions] *********************************************************************************************************************************************************************************************************************************************ok: [k8smaster] => {                                                                                                                                                                                                                                                          
    "result.stdout_lines": [                                                                                                           
        "Dernière vérification de l’expiration des métadonnées effectuée il y a 0:02:38 le mar. 23 nov. 2021 10:16:41 UTC.",           
        "Paquets installés",                                                                                                                                                                                                                                                  
        "kubeadm.x86_64                       1.19.15-0                       @kubernetes",                                            
        "Paquets disponibles",                                                                                                                                                                                                                                                
        "kubeadm.x86_64                       1.6.0-0                         kubernetes ",                                            
...
        "kubeadm.x86_64                       1.19.9-0                        kubernetes ",                                            
        "kubeadm.x86_64                       1.19.10-0                       kubernetes ",                                                                                                                                                                                   
        "kubeadm.x86_64                       1.19.11-0                       kubernetes ",                                                                                                                                                                                   
        "kubeadm.x86_64                       1.19.12-0                       kubernetes ",                                                                                                                                                                                   
        "kubeadm.x86_64                       1.19.13-0                       kubernetes ",                                            
        "kubeadm.x86_64                       1.19.14-0                       kubernetes ",                                                                                                                                                                                   
        "kubeadm.x86_64                       1.19.15-0                       kubernetes ",                                                                                                                                                                                   
        "kubeadm.x86_64                       1.19.16-0                       kubernetes ",                                                                                                                                                                                   
        "kubeadm.x86_64                       1.20.0-0                        kubernetes ",                                            
        "kubeadm.x86_64                       1.20.1-0                        kubernetes ",                                            
        "kubeadm.x86_64                       1.20.2-0                        kubernetes ",                                                                                                                                                                                   
        "kubeadm.x86_64                       1.20.4-0                        kubernetes ",                                            
        "kubeadm.x86_64                       1.20.5-0                        kubernetes ",                                            
        "kubeadm.x86_64                       1.20.6-0                        kubernetes ",                                                                                                                                                                           
        "kubeadm.x86_64                       1.20.7-0                        kubernetes ",                                                                                                                                                                           
        "kubeadm.x86_64                       1.20.8-0                        kubernetes ",                                                                                                                                                                                   "kubeadm.x86_64                       1.20.9-0                        kubernetes ",                                                                                                                                                                           
        "kubeadm.x86_64                       1.20.10-0                       kubernetes ",                                                                                                                                                                           
        "kubeadm.x86_64                       1.20.11-0                       kubernetes ",                                                                                                                                                                           
        "kubeadm.x86_64                       1.20.12-0                       kubernetes ",                                                                                                                                                                           
        "kubeadm.x86_64                       1.20.13-0                       kubernetes ",                                                                                                                                                                           
        "kubeadm.x86_64                       1.21.0-0                        kubernetes ",                                                                                                                                                                           
        "kubeadm.x86_64                       1.21.1-0                        kubernetes ",                                                                                                                                                                           
        "kubeadm.x86_64                       1.21.2-0                        kubernetes ",                                                                                                                                                                           
        "kubeadm.x86_64                       1.21.3-0                        kubernetes ",                                                                                                                                                                           
        "kubeadm.x86_64                       1.21.4-0                        kubernetes ",                                                                                                                                                                           
        "kubeadm.x86_64                       1.21.5-0                        kubernetes ",                                                                                                                                                                           
        "kubeadm.x86_64                       1.21.6-0                        kubernetes ",                                                                                                                                                                           
        "kubeadm.x86_64                       1.21.7-0                        kubernetes ",                                                                                                                                                                           
        "kubeadm.x86_64                       1.22.0-0                        kubernetes ",                                                                                                                                                                           
        "kubeadm.x86_64                       1.22.1-0                        kubernetes ",                                                                                                                                                                           
        "kubeadm.x86_64                       1.22.2-0                        kubernetes ",                                                                                                                                                                           
        "kubeadm.x86_64                       1.22.3-0                        kubernetes ",                                                                                                                                                                           
        "kubeadm.x86_64                       1.22.4-0                        kubernetes "                                                                                                                                                                            
    ]                                                                                                                                                                                                                                                                 
}                                                                                                                                                                                                                                                                     
                                                                                                                                                                                                                                                                                
PLAY RECAP *******************************************************************************************************************************************************************************************************************************************************************
k8smaster                  : ok=3    changed=1    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0                                                                                                                                              

```

   Voilà la version de départ de notre **cluster**


```code

ubuntu:~/ansible/playbooks/microk8s$ ssh k8smaster "sudo kubectl get nodes"                                                                                                                                                                                                
NAME          STATUS   ROLES    AGE     VERSION                                                                                                                                                                                                                                 
k8smaster     Ready    master   5m12s   v1.19.15                                                                                                                                                                                                                                
k8sworker-1   Ready    worker   2m9s    v1.19.15                                                                                                                                                                                                                                
```
<br>

Nous allons monter de version vers la derniére release *1.20* soit la *1.20.13-0*  le but sera d'aller vers la version *1.22*

Démarrer la mise à jour avec cette commande, renseigner la variable avec le numéro de release



```code 
ubuntu:~/ansible/playbooks/microk8s$ ansible-playbook -i inventory/hosts playbooks/update_master.yml --extra-vars "version=1.20.13-0" --tags update_master                                                                                                                 
                                                                                                                                                                                                                                                                                
PLAY [master] ****************************************************************************************************************************************************************************************************************************************************************  
                                                                                                                                                                                                                                                                                
TASK [Gathering Facts] *******************************************************************************************************************************************************************************************************************************************************  
ok: [k8smaster]                                                                                                                                                                                                                                                                 
                                                                                                                                                                                                                                                                                
TASK [Update k8smaster to 1.xx to 1.xx] **************************************************************************************************************************************************************************************************************************************  
changed: [k8smaster]                                                                                                                                                                                                                                                            
                                                                                                                                                                                                                                                                                
PLAY RECAP *******************************************************************************************************************************************************************************************************************************************************************  
k8smaster                  : ok=2    changed=1    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0                                                                                                                                  

```

Voilà le master Kubernetes est mis à jour **control plane** et RPM's **kubernetes**.

```code
ubuntu:~/ansible/playbooks/microk8s$ ssh k8smaster "sudo kubectl get nodes"                                                                                                                                                                                                
NAME          STATUS   ROLES    AGE   VERSION                                                                                                                                                                                                                                   
k8smaster     Ready    master   16m   v1.20.13                                                                                          
k8sworker-1   Ready    worker   13m   v1.19.15                                                                                

```    
`NOTE` vous remarquerez que seul le **master** a été mis à jour. Poursuivre avec le **worker**.


```code
ubuntu:~/ansible/playbooks/microk8s$ ansible-playbook -i inventory/hosts playbooks/update_worker.yml --extra-vars "version=1.20.13-0" --tags update_worker                                                                                                                 
                                                                                                                                                                                                                                                                                
PLAY [worker] ****************************************************************************************************************************************************************************************************************************************************************  
                                                                                                                                                                                                                                                                                
TASK [Gathering Facts] *******************************************************************************************************************************************************************************************************************************************************  
ok: [k8sworker-1]                                                                                                                                                                                                                                                               
                                                                                                                                                                                                                                                                                
TASK [Update k8smaster to 1.xx to 1.xx] **************************************************************************************************************************************************************************************************************************************  
changed: [k8sworker-1]                                                                                                                  

```

Cette fois nous sommes dans les mêmes versions, recommencer avec la version suivante.

```code

ubuntu:~/ansible/playbooks/microk8s$ ssh k8smaster "sudo kubectl get nodes"                                                                                                                                                                                                
NAME          STATUS   ROLES    AGE   VERSION                                                                                                                                                                                                                                   
k8smaster     Ready    master   32m   v1.20.13                                                                                                                                                                                                                                  
k8sworker-1   Ready    worker   29m   v1.20.13                                      
```

Il reste encore une étape pour la mise à jour des instances et de mettre à jour celles-ci d'un point de vu *Operating System*. Pour ce faire il faut désinstaler **kubelet** procéder à un update **OS** puis réinstaller les binaires **kubernetes** comme *kubelet*,*kubectl* dans les versions de destination.


<br>
<br>

-----
RESUM
-----

<p>


<br>

-----
PURGE
-----

ansible-playbook -i inventory/hosts.yml setup_openstack.yml --tags purge_rancher,purge_port_rancher


ansible-playbook -i inventory/hosts.yml setup_openstack.yml --tags purge_master,purge_node,purge_port_master,purge_port_node,purge_router,purge_network

<br>

---------
OPENSTACK
---------


ansible-playbook -i inventory/hosts.yml setup_openstack.yml --extra-vars "type=master" --tags security_group


ansible-playbook -i inventory/hosts.yml setup_openstack.yml --extra-vars "type=master" --tags network,subnet,router,port,create_floating_ip,instance,assign_floating_ip


ansible-playbook -i inventory/hosts.yml setup_openstack.yml  --extra-vars "type=node" --tags network,subnet,router,port,create_floating_ip,instance,create_floating_ip


ansible-playbook -i inventory/hosts.yml setup_openstack.yml --tags port_rancher,rancher,assign_floating_ip_rancher

 
<br>

---------
SSH-KEYGEN
---------

ssh-keygen -f "/home/ange/.ssh/known_hosts" -R "k8smaster"   ; ssh-keygen -f "/home/ange/.ssh/known_hosts" -R "195.78.23.57"  ; ssh k8smaster "df" ;   \
ssh-keygen -f "/home/ange/.ssh/known_hosts" -R "k8sworker-1" ; ssh-keygen -f "/home/ange/.ssh/known_hosts" -R "195.78.22.116" ; ssh k8sworker-1 "df" ; \
ssh-keygen -f "/home/ange/.ssh/known_hosts" -R "k8sworker-2" ; ssh-keygen -f "/home/ange/.ssh/known_hosts" -R "195.78.22.129" ; ssh k8sworker-2 "df" ; \
ssh-keygen -f "/home/ange/.ssh/known_hosts" -R "k8sworker-3" ; ssh-keygen -f "/home/ange/.ssh/known_hosts" -R "195.78.22.47"  ; ssh k8sworker-3 "df"


ssh-keygen -f "/home/ange/.ssh/known_hosts" -R "rancher-1"   ; ssh-keygen -f "/home/ange/.ssh/known_hosts" -R "195.78.23.171" ; ssh rancher-1 "df"


ansible -i inventory/hosts master,worker,rancher -m ping


<br>

---------
SETUP
---------

ansible-playbook -i inventory/hosts setup_docker.yml


ansible-playbook -i inventory/hosts setup_master.yml


ansible-playbook -i inventory/hosts setup_worker.yml


ansible-playbook -i inventory/hosts setup_rancher.yml


ansible-playbook -i inventory/hosts setup_rancher.yml --tags pass


cat rancher_pass


<br>

----
MISE A JOUR
----

ansible-playbook -i inventory/hosts playbooks/update_master.yml --tags display_versions

ssh k8smaster "sudo kubectl get nodes"

ansible-playbook -i inventory/hosts playbooks/update_master.yml --extra-vars "version=1.20.13-0" --tags update_master 


ansible-playbook -i inventory/hosts playbooks/update_worker.yml --extra-vars "version=1.20.13-0" --tags update_worker 

ansible-playbook -i inventory/hosts playbooks/update_master.yml --extra-vars "version=1.21.7-0" --tags update_master 


ansible-playbook -i inventory/hosts playbooks/update_worker.yml --extra-vars "version=1.21.7-0" --tags update_worker 

ansible-playbook -i inventory/hosts playbooks/update_master.yml --extra-vars "version=1.22.4-0" --tags update_master 


ansible-playbook -i inventory/hosts playbooks/update_worker.yml --extra-vars "version=1.22.4-0" --tags update_worker 





systemctl stop docker
iptables –flush
iptables -tnat –flush
systemctl start docker









<p>

